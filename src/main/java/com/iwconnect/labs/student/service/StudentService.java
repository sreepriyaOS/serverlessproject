package com.iwconnect.labs.student.service;

import com.iwconnect.labs.student.domain.Student;
import com.iwconnect.labs.student.dto.StudentPojo;
import com.iwconnect.labs.student.repository.StudentRepository;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public interface StudentService {


	public Student saveStudent(StudentPojo responseData) throws Exception;


	/********************getStudentList-Start********************/

	public  List<StudentPojo> getStudentList();

		/********************getStudentList-End********************/



}