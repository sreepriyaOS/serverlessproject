package com.iwconnect.labs.student.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iwconnect.labs.student.domain.Student;
import com.iwconnect.labs.student.dto.StudentPojo;
import com.iwconnect.labs.student.repository.StudentRepository;
import com.iwconnect.labs.student.service.StudentService;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {
	private static Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

	@Autowired
	StudentRepository studentRepository;

	@Override
	public Student saveStudent(StudentPojo studentPojo) throws Exception {
		logger.info("Saving student {} into DB", studentPojo);
		Student transientStudent = new Student();
		try {
			// mapping attributes
			transientStudent.setFirstName(studentPojo.getFirstName());
			transientStudent.setLastName(studentPojo.getLastName());
			// save to DB
			return studentRepository.save(transientStudent);
		} catch (Exception e) {
			logger.error("Error saving student ", e);
			throw e;
		}
	}

	@Override
	public List<StudentPojo> getStudentList() {
		List<Student> all= (List<Student>) studentRepository.findAll();
		List<StudentPojo> studentPojos=new ArrayList<>();
		for(Student students:all){
			StudentPojo studentPojo=new StudentPojo();
			studentPojo.setFirstName(students.getFirstName());
			studentPojo.setLastName(students.getLastName());
			studentPojos.add(studentPojo);
		}
		return studentPojos;
	}


//	//*******.To use gateway rest API with multiple end points- end *******//


}